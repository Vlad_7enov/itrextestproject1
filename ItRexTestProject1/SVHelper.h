//
//  SVHelper.h
//  ItRexTestProject1
//
//  Created by Vladsilav Semenov on 22.06.16.
//  Copyright © 2016 Vladislav Semenov. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *SVHelperAddress;
extern NSString *SVHelperAddressStreet;
extern NSString *SVHelperAddressState;
extern NSString *SVHelperAddressCity;
extern NSString *SVHelperAddressLandlord;
extern NSString *SVHelperAddressPrice;
extern NSString *SVHelperEmptyString;

@interface SVHelper : NSObject

@end
