//
//  SVAddressDetailViewController.m
//  ItRexTestProject1
//
//  Created by Vladsilav Semenov on 22.06.16.
//  Copyright © 2016 Vladislav Semenov. All rights reserved.
//

#import "SVAddressDetailViewController.h"
#import "SVDataBaseManager.h"
#import "SVHelper.h"
#import "SVAddressMO.h"

@interface SVAddressDetailViewController () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UIButton *deleteButton;
@property (nonatomic, weak) IBOutlet UITextField *streetTextField;
@property (nonatomic, weak) IBOutlet UITextField *cityTextField;
@property (nonatomic, weak) IBOutlet UITextField *stateTextField;
@property (nonatomic, weak) IBOutlet UITextField *priceTextField;
@property (nonatomic, weak) IBOutlet UITextField *landlordTextField;
@property (nonatomic, strong) UIBarButtonItem *saveBarButtonItem;

@end

@implementation SVAddressDetailViewController

#pragma mark - View life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.address) {
        self.deleteButton.hidden = NO;
        self.streetTextField.text = self.address.street;
        self.cityTextField.text = self.address.city;
        self.stateTextField.text = self.address.state;
        self.landlordTextField.text = self.address.landlord;
        self.priceTextField.text = self.address.price.stringValue;
    }
    
    UIBarButtonItem *cancelButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(didPressCancelButton:)];
    self.navigationItem.leftBarButtonItem = cancelButtonItem;
}

#pragma mark - Managing addresses

- (void)create {
    NSNumberFormatter *numberFormattter = [[NSNumberFormatter alloc] init];
    NSNumber *price = [numberFormattter numberFromString:self.priceTextField.text];
    
    NSDictionary *addressInfo = @{
                                  SVHelperAddressStreet : self.streetTextField.text,
                                  SVHelperAddressCity : self.cityTextField.text,
                                  SVHelperAddressState : self.stateTextField.text,
                                  SVHelperAddressPrice : price,
                                  SVHelperAddressLandlord : self.landlordTextField.text
                                  };
    
    
    SVAddressMO *addressToAdd = [[SVDataBaseManager sharedManager] createEntityWithClassName:SVHelperAddress attributesDictionary:addressInfo];
    
    [[SVDataBaseManager sharedManager] saveContext];
}

- (void)update {
    NSNumberFormatter *numberFormattter = [[NSNumberFormatter alloc] init];
    NSNumber *price = [numberFormattter numberFromString:self.priceTextField.text];
    
    self.address.street = self.streetTextField.text;
    self.address.city = self.cityTextField.text;
    self.address.state = self.stateTextField.text;
    self.address.price = price;
    self.address.landlord = self.landlordTextField.text;
    
    [[SVDataBaseManager sharedManager] saveContext];
}

- (BOOL)checkFields {
    
    if ([self.streetTextField.text isEqualToString:SVHelperEmptyString] ||
        [self.cityTextField.text isEqualToString:SVHelperEmptyString] ||
        [self.stateTextField.text isEqualToString:SVHelperEmptyString] ||
        [self.priceTextField.text isEqualToString:SVHelperEmptyString] ||
        [self.landlordTextField.text isEqualToString:SVHelperEmptyString]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Warning" message:@"All fields must not be empty" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
        return NO;
    }
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    NSNumber *price = [numberFormatter numberFromString:self.priceTextField.text];
    
    if (!price) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Price must be a number" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
        return NO;
    }
    
    return YES;
}

#pragma mark - Actions

- (void)didPressSaveButton:(id)sender {
    
    if (![self checkFields]) {
        return;
    }
    
    if (self.address) {
        [self update];
    }
    else {
        [self create];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didPressCancelButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didPressDeleteButton:(UIButton *)sender {
    [[SVDataBaseManager sharedManager] deleteEntity:self.address];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITextFieldDelegate implementation

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.saveBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(didPressSaveButton:)];
    self.navigationItem.rightBarButtonItem = self.saveBarButtonItem;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return YES;
}

@end
