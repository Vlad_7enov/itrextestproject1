//
//  CALayer+BorderColor.m
//  SnappySweeps
//
//  Created by Vladsilav Semenov on 23.04.16.
//  Copyright © 2016 Balina Soft. All rights reserved.
//

#import "CALayer+BorderColor.h"

@implementation CALayer (BorderColor)

-(void)setBorderUIColor:(UIColor*)color
{
    self.borderColor = color.CGColor;
}

-(UIColor*)borderUIColor
{
    return [UIColor colorWithCGColor:self.borderColor];
}

@end
