//
//  AppDelegate.m
//  ItRexTestProject1
//
//  Created by Vladsilav Semenov on 22.06.16.
//  Copyright © 2016 Vladislav Semenov. All rights reserved.
//

#import "SVAppDelegate.h"
#import "SVDataBaseManager.h"

@interface SVAppDelegate ()

@end

@implementation SVAppDelegate

#pragma marl - App delegate

- (void)applicationWillTerminate:(UIApplication *)application {
    [[SVDataBaseManager sharedManager] saveContext];
}

@end
