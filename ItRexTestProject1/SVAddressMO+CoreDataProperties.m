//
//  SVAddressMO+CoreDataProperties.m
//  ItRexTestProject1
//
//  Created by Vladsilav Semenov on 22.06.16.
//  Copyright © 2016 Vladislav Semenov. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SVAddressMO+CoreDataProperties.h"

@implementation SVAddressMO (CoreDataProperties)

@dynamic street;
@dynamic city;
@dynamic state;
@dynamic price;
@dynamic landlord;

@end
