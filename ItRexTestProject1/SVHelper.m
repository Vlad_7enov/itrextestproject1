//
//  SVHelper.m
//  ItRexTestProject1
//
//  Created by Vladsilav Semenov on 22.06.16.
//  Copyright © 2016 Vladislav Semenov. All rights reserved.
//

#import "SVHelper.h"

NSString *SVHelperAddress = @"Address";
NSString *SVHelperAddressStreet = @"street";
NSString *SVHelperAddressState = @"state";
NSString *SVHelperAddressCity = @"city";
NSString *SVHelperAddressLandlord = @"landlord";
NSString *SVHelperAddressPrice = @"price";
NSString *SVHelperEmptyString = @"";

@implementation SVHelper

@end
