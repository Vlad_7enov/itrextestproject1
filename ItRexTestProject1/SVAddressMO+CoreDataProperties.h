//
//  SVAddressMO+CoreDataProperties.h
//  ItRexTestProject1
//
//  Created by Vladsilav Semenov on 22.06.16.
//  Copyright © 2016 Vladislav Semenov. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SVAddressMO.h"

NS_ASSUME_NONNULL_BEGIN

@interface SVAddressMO (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *street;
@property (nullable, nonatomic, retain) NSString *city;
@property (nullable, nonatomic, retain) NSString *state;
@property (nullable, nonatomic, retain) NSNumber *price;
@property (nullable, nonatomic, retain) NSString *landlord;

@end

NS_ASSUME_NONNULL_END
