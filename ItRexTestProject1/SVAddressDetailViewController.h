//
//  SVAddressDetailViewController.h
//  ItRexTestProject1
//
//  Created by Vladsilav Semenov on 22.06.16.
//  Copyright © 2016 Vladislav Semenov. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SVAddressMO;

@interface SVAddressDetailViewController : UIViewController

@property (nonatomic, strong) SVAddressMO *address;

@end
